import firebase from "firebase/app";

const config = {
  apiKey: process.env.API_KEY,
  authDomain: process.env.AUTH_DOMAIN,
  databaseURL: process.env.DATABASE_URL,
  projectId: process.env.PROJECT_ID,
  storageBucket: process.env.STORAGE_BUCKET,
  messagingSenderId: process.env.MESSAGING_SENDER_ID
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

// firebase.auth().onAuthStateChanged(user => {
//   if (typeof window !== 'undefined') {
//     console.log('ログイン状態が変更されました。')
//     console.log(user)
//   }
// })

class FirebaseUtil {
  initUser() {
    let loggedInUser;
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        loggedInUser = user;
      } else {
        loggedInUser = "";
      }
    });
    console.log("初期ユーザーを取得します。");
    console.log(loggedInUser);
    return loggedInUser;
  }

  loggedIn(email, password) {
    if (!firebase.auth().currentUser) {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .catch(error => {
          let errorCode = error.code;
          let errorMessage = error.message;
          if (errorCode === "auth/wrong-password") {
            alert("Wrong password.");
          } else {
            alert(errorMessage);
          }
          console.log(error);
        });
    }

    console.log(firebase.auth().currentUser);
    return firebase.auth().currentUser;
  }

  signOut() {
    if (firebase.auth().currentUser) {
      firebase.auth().signOut();
    }

    console.log(firebase.auth().currentUser);
    return firebase.auth().currentUser;
  }

  toggleSignIn(email, password) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        firebase.auth().signOut();
      } else {
        firebase
          .auth()
          .signInWithEmailAndPassword(email, password)
          .catch(error => {
            let errorCode = error.code;
            let errorMessage = error.message;
            if (errorCode === "auth/wrong-password") {
              alert("Wrong password.");
            } else {
              alert(errorMessage);
            }
            console.log(error);
          });
      }
    });

    // if (firebase.auth().currentUser) {
    //   firebase.auth().signOut()
    // } else {
    //   firebase.auth().signInWithEmailAndPassword(email, password).catch(error => {
    //     let errorCode = error.code
    //     let errorMessage = error.message
    //     if (errorCode === 'auth/wrong-password') {
    //       alert('Wrong password.')
    //     } else {
    //       alert(errorMessage)
    //     }
    //     console.log(error)
    //   });
    // }

    return firebase.auth().currentUser;
  }

  register(email, password) {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .catch(error => {
        let errorCode = error.code;
        let errorMessage = error.message;
        if (errorCode == "auth/weak-password") {
          alert("The password is too weak.");
        } else {
          alert(errorMessage);
        }
        console.log(error);
      });

    return firebase.auth().currentUser;
  }

  activate() {
    firebase
      .auth()
      .currentUser.sendEmailVerification()
      .then(() => {
        alert("Email Verification Sent!");
      });
  }

  deleteUser() {
    console.log("退会処理をします");
    let user = firebase.auth().currentUser;
    console.log(user.email + "を退会させます");
    user
      .delete()
      .then(() => {
        // User deleted.
        console.log("削除に成功しました");
      })
      .catch(error => {
        // An error happened.
        console.log(error.message);
      });
  }

  // register(email, password, router) {
  //   console.log('サインアップします。')
  //   firebase.auth().createUserWithEmailAndPassword(email, password).then(res => {
  //     console.log('Create account: ', res.user.email)
  //     console.log('サインアップに成功しました。')
  //     console.log('ログインします。')
  //     console.log('Logged in account: ', res.user.email)
  //     console.log('ログインに成功しました。')
  //     console.log('メール確認: ' + res.user.emailVerified)
  //     if (!res.user.emailVerified) {
  //       console.log('確認メールを送信します')
  //       this.sendEmailVerification()
  //       console.log('確認メールを送信しました')
  //     }
  //     router.push('/')
  //     // firebase.auth().signInWithEmailAndPassword(res.user.email, res.user.password).then(res => {
  //     //   current = firebase.auth().currentUser()
  //     //   localStorage.setItem('jwt', res.user.qa)
  //     //   this.$router.push('/')
  //     // }, err => {
  //     //   alert(err.message)
  //     // })
  //   }).catch( error => {
  //     console.log('エラー: ' + error.message)
  //     console.log('エラーをキャッチしました')
  //   })
  // }

  sendEmailVerification() {
    firebase
      .auth()
      .currentUser.sendEmailVerification()
      .catch(error => {
        console.log("エラー: " + error.message);
        console.log("エラーをキャッチしました");
      });
  }
}

export default (context, inject) => {
  inject("firebase", new FirebaseUtil());
};
